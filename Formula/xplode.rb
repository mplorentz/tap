class Xplode < Formula
  desc "The script to reset your Xcodes"
  homepage "https://gitlab.com/mplorentz/xplode"
  url "https://gitlab.com/mplorentz/xplode/-/archive/1.0.1/xplode-1.0.1.tar.gz"
  sha256 "70f87b31c76c4c72ee73839afb7f2ba7ff9d1e35684027c06f66e6a74a02bafe"

  def install
    bin.install "xplode"
  end

  test do
    system "false"
  end
end
